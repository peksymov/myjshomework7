alert('DOM – это объектная модель документа, которую браузер создаёт в памяти компьютера на основании HTML-кода, полученного им от сервера.\n' +
    '\n' +
    'HTML-код – это текст страницы, а DOM – это набор связанных объектов, созданных браузером при парсинге её текста.');
// вариант без ul
// function browserList (arrayQ) {
//     return arrayQ.map(item => `<li>${item}</li>`).join('');
// }
// document.write(browserList([12,34,54,'строка','еще одна строка',45 ,45]));


function browser (arrayQ) {
    return arrayQ.map(item => `<li>${item}</li>`).join('');
}
const str = `<ul>` + `${browser([12,34,54,'frnjfnr','jntvt',45 ,45])}` + `</ul>`
document.write(str);



